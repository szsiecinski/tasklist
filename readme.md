# TaskList
Lista zadań w Django

**Autorzy**: Szymon Sieciński, Klaudia Kłoniecka, Agnieszka Kuźniak

## Wymagania
* Python 3.4 +
* Django 1.8 +
* SQLite 3+

## Jak uruchamiać?
Polecenie runserver Django
> python manage.py runserver

Domyślnie uruchomi się aplikacja działająca na http://localhost:8000

## Generowanie bazy danych
Polecenie migrate Django

> python manage.py migrate

## Do zrobienia
* ~~Dodanie filtru zadań do zrobienia (wyświetlanie wyłącznie zadań do zrobienia)~~
* ~~Ostylowanie z Bootstrap 3~~
* ~~Wyświetlanie w tabeli opisu zadań. W nagłówku kolumny zgodnie z modelem~~
